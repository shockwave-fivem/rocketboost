function IsValidVehicle(vehicle)
  return vehicle and vehicle ~= 0 or false
end

function GetVehicleDriver(vehicle)
  return GetPedInVehicleSeat(vehicle, -1)
end

function CanPedUseVehicleRocket(ped, vehicle)
  return (
    IsValidVehicle(vehicle) and
    GetIsVehicleEngineRunning(vehicle) and
    HasVehicleRocketBoost(vehicle) and
    GetVehicleDriver(vehicle) == ped
  )
end
